'use strict';

angular.module('newsApp')
    .controller('followNewsCtrl', ['$scope', '$http', function($scope, $http) {
        $http.get('http://news-world.iiar.pwr.edu.pl/api/v1/users/me/news/followed_people/', {
            headers: {
                Authorization: 'Token ' + sessionStorage.getItem('token')
            }
        }).success(function(response) {
            $scope.followNews = response.news;
        });

        $scope.clearInput = function() {
            $scope.searchFilter = "";
        };

        $http.get('http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').success(function(response) {
            $scope.topCategories = response.user.categories;
        });
    }]);
