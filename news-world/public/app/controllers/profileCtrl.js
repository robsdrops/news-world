'use strict';

angular.module('newsApp')
    .controller('profileCtrl', ['$http', '$scope', function($http, $scope) {

        $scope.model = [];

        $http.get('http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').success(function(response) {
            $scope.userData = response.user;
            sessionStorage.setItem('email', response.user.email);
        });

        $http.get('http://news-world.iiar.pwr.edu.pl/api/v1/categories/').success(function(response) {
            $scope.Categories = response.categories;
            $scope.model = (new Array($scope.Categories.length)).fill(false);
        });

        $http.get('http://news-world.iiar.pwr.edu.pl/api/v1/users/me/followed_people/').success(function(response) {
            $scope.followedPeople = response.followed_people;
            console.log($scope.followedPeople[0].first_name);
        });

        $scope.editEmail = function(new_email) {
            if (sessionStorage.getItem('email') !== new_email) {
                $http.put('http://news-world.iiar.pwr.edu.pl/api/v1/users/me/', {
                    user: {
                        email: new_email
                    }
                }).success(function() {
                    swal("Success!", "You successfully changed your email adress", "success");
                }).error(function() {
                    swal("Some Error!", "You need to fix your mistake", "error");
                });
            } else {
                swal("Error!", "You chose the same email!", "error");
            };

        };

        $scope.editPassword = function(current, new_password, new_passwordRepeat) {
            if (current !== new_password) {
                $http.post('http://news-world.iiar.pwr.wroc.pl/api/v1/users/me/password/', {
                    user: {
                        original_password: current,
                        new_password: new_password,
                        new_passwordRepeat
                    }
                }, {
                    headers: {
                        Authorization: 'Token ' + sessionStorage.getItem('token')
                    }
                }).success(function() {
                    swal("Success!", "You successfully changed your password", "success");
                }).error(function() {
                    swal("Some Error!", "Problem with communication to the server", "error");
                });
            } else {
                swal("Error!", "You chose the same password!", "error");
            };
        };

        $scope.editCategory = function(model) {
            var zaznaczoneId = [];
            $scope.model.forEach(function(v, i) {
                if (v) zaznaczoneId.push($scope.Categories[i].id);
            });

            $http.put('http://news-world.iiar.pwr.edu.pl/api/v1/users/me/', {
                user: {
                    category_ids: zaznaczoneId
                }
            }, {
                headers: {
                    Authorization: 'Token ' + sessionStorage.getItem('token')
                }
            }).success(function() {
                if (zaznaczoneId != "") {
                    swal("", "Your category has been changed", "success")
                } else {
                    console.log('zero zmian kategorii');
                }
            });

        };

        $scope.followPerson = function(name, lastname, profession) {
            $http.post('http://news-world.iiar.pwr.edu.pl/api/v1/users/me/followed_people/', {
                user: {
                    followed_person: {
                        first_name: name,
                        second_name: lastname,
                        profession: {
                            name: profession
                        }
                    }
                }
            }, {
                headers: {
                    Authorization: 'Token ' + sessionStorage.getItem('token')
                }
            }).success(function() {
                swal("Success!", "", "success");
            }).error(function() {
                swal("Error!", "", "error");
            });
        };

        $scope.editPerson = function(edit_name, edit_second_name, edit_profession, followId) {
            for (var i = 0; i < $scope.followedPeople.length; i++) {
                if ($scope.followedPeople[i].first_name == edit_name ||
                    $scope.followedPeople[i].second_name == edit_second_name ||
                    $scope.followedPeople[i].profession.name == edit_profession) {
                    $http.put('http://news-world.iiar.pwr.edu.pl/api/v1/users/me/followed_people/' + $scope.followedPeople[i].id, {
                        user: {
                            followed_person: {
                                first_name: edit_name,
                                second_name: edit_second_name,
                                profession: {
                                    name: edit_profession
                                }
                            }
                        }
                    }, {
                        headers: {
                            Authorization: 'Token ' + sessionStorage.getItem('token')
                        }
                    }).success(function() {
                        swal("Success!", "", "success");
                    }).error(function() {
                        swal("Error!", "", "error");
                    });
                }
            }
        };


        $scope.isChecked = function(model) {
            return $scope.model.every(function(e) {
                return e === false;
            });
        };
    }]);


//if ($scope.followPerson.first_name !== name && $scope.followPerson.second_name !== lastname && $scope.followPerson.profession !== profession) {
