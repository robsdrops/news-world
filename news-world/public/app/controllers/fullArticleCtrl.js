'use strict';

angular.module('newsApp')
	.controller('fullArticleCtrl', ['$scope', '$http', '$stateParams', function ($scope, $http, $stateParams) {

		$http.get('http://news-world.iiar.pwr.edu.pl/api/v1/news/' + $stateParams.articleId).success(function(response){
			$scope.article = response.news;
            
            $scope.c = document.getElementById("c");
            $scope.ctx = $scope.c.getContext("2d");
            
            $scope.ctx.font = "15px Arial";
            $scope.ctx.fillStyle = "#808080";
            $scope.ctx.fillText("Likes: " + $scope.article.up_votes,0,15);
            $scope.ctx.fillText("Dislikes: " + $scope.article.down_votes,0,45);
            
            $scope.l_length=$scope.article.up_votes*(150/($scope.article.up_votes+$scope.article.down_votes));
            $scope.d_length=$scope.article.down_votes*(150/($scope.article.up_votes+$scope.article.down_votes));
            
            $scope.ctx.fillStyle = "#607060";
            $scope.ctx.fillRect(110, 1, 150, 15);
            $scope.ctx.fillStyle = "#40f060";
            $scope.ctx.fillRect(110, 1, $scope.l_length, 15);
            $scope.ctx.fillStyle = "#705050";
            $scope.ctx.fillRect(110, 30, 150, 15);
            $scope.ctx.fillStyle = "#f04040";
            $scope.ctx.fillRect(110, 30, $scope.d_length, 15);
		});

		$scope.backTo = function() {
			window.history.back();
		};

		$scope.like = function(){
			if($scope.article.voted==null){
				$http.post('http://news-world.iiar.pwr.edu.pl/api/v1/news/' + $stateParams.articleId + '/votes', 
				{
					vote: { 
						value: true
					}
				}).then(function(response){
					swal({ title: "You like this!", timer: 1500,type: "success", showConfirmButton: false});
				    $scope.article.voted = true;
				}).catch(function(){

				});
			} else if($scope.article.voted==false) {
				$http.put('http://news-world.iiar.pwr.edu.pl/api/v1/news/' + $stateParams.articleId + '/votes', 
				{
					vote: { 
						value: true
					}
				}).then(function(){
					swal({ title: "You like this!", timer: 1500,type: "success", showConfirmButton: false});
				    $scope.article.voted = true;
				}).catch(function(){

				});
			}
			else{
				console.log("nie mozna glosowac 2 razy");
			}
		};

		$scope.dislike = function(){
			if($scope.article.voted==null){
				$http.post('http://news-world.iiar.pwr.edu.pl/api/v1/news/' + $stateParams.articleId + '/votes', 
				{
					vote: { 
						value: false
					}
				}).then(function(){
					swal({ title: "You dislike this!", timer: 1500,type: "error", showConfirmButton: false});
					$scope.article.voted = false;
					console.log("wyslano po raz 1 - dislike");
				}).catch(function(){
					console.log("niewyslano po raz 1 - dislike");
				});
			} else if($scope.article.voted==true) {
				$http.put('http://news-world.iiar.pwr.edu.pl/api/v1/news/' + $stateParams.articleId + '/votes', 
				{
					vote: { 
						value: false
					}
				}).then(function(){
					swal({ title: "You dislike this!", timer: 1500,type: "error", showConfirmButton: false});
				$scope.article.voted = false;
				}).catch(function(){

				});
			}
			else{
				console.log("nie mozna glosowac 2 razy");
			}
		};    
        
	}]);