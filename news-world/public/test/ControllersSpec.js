describe("controller", function() {
    'use strict';
    
    var p, $scope, $location, $controller, ValidationService, $httpBackend, $q, $stateParams;
    
    var newsAppMock=angular.module('newsAppMock', []);
    newsAppMock.service('ValidationService', function($q) {
        this.isSignedIn=function() {
            var promise=$q.defer();
            promise.resolve({role: 'user'});
            return promise.promise;
        }
        
        this.signUp=function(){
            return p.promise;
        }
        
        this.signIn=function(login, password){
            return p.promise;
        }
        
        this.setUserData=function(user) {
            this.userData=user;
        }
    });
    
    beforeEach(function() {
        module('newsApp');
        module('newsAppMock');
        inject(function($rootScope, _$location_, _$controller_, _ValidationService_, _$httpBackend_, _$q_, _$stateParams_){
            $scope = $rootScope.$new();
            $location = _$location_;
            $controller = _$controller_;
            ValidationService = _ValidationService_;
            $httpBackend = _$httpBackend_;
            $q = _$q_;
            $stateParams = _$stateParams_;
        });
    });
    
    describe('adminCtrl', function() {
        
        it('should route to /login if users role is not admin', function() {
            
            ValidationService.setUserData({role: 'user'});
            var ctrl = $controller('adminCtrl', {$scope : $scope});
            expect($location.path()).toEqual('/login');
        }) 
        
        it('should stay on the same site if users role is admin', function() {
            
            ValidationService.setUserData({role: 'admin'});
            var ctrl = $controller('adminCtrl', {$scope : $scope});
            expect($location.path()).toEqual('');
        })
        
        it('should set admin menu', function() {
            
            ValidationService.setUserData({role: 'admin'});
            var ctrl = $controller('adminCtrl', {$scope : $scope});
            
            var menu = [
                {
                    icon: 'fa fa-newspaper-o',
                    state: 'admin.news',
                    text: 'General News'
                },

                {
                    icon: 'fa fa-wrench',
                    state: 'admin.manageArticles',
                    text: 'Manage Articles'
                },

                {
                    icon: 'fa fa-users',
                    state: 'admin.manageUsers',
                    text: 'Manage Users'
                },

                {	
                    icon: 'fa fa-times-circle',
                    state: 'logout',
                    text: 'Logout'
                }
		    ];
            expect($scope.adminMenu).toEqual(menu);
        })
    })
    
    describe('userCtrl', function() {
        
        it('should route to /login if users role is not user', function() {
            
            ValidationService.setUserData({role: 'admin'});
            var ctrl = $controller('userCtrl', {$scope : $scope});
            expect($location.path()).toEqual('/login');
        })
        
        it('should stay on the same site if users role is user', function() {

            ValidationService.setUserData({role: 'user'});
            var ctrl = $controller('userCtrl', {$scope : $scope});
            expect($location.path()).toEqual('');
        })
        
        it('should set user menu', function() {
            
            ValidationService.setUserData({role: 'user'});
            var ctrl = $controller('userCtrl', {$scope : $scope});
        
            var menu = [
                {
                    icon: 'fa fa-trophy',
                    state: 'user.topnews',
                    text: 'Top10 News'
                },

                {
                    icon: 'fa fa-heart-o',
                    state: 'user.mynews',
                    text: 'My News'
                },

                {
                    icon: 'fa fa-newspaper-o',
                    state: 'user.news',
                    text: 'General News'
                },

                {
                    icon: 'fa fa-user',
                    state: 'user.profile',
                    text: 'My profile'
                },

                {	
                    icon: 'fa fa-times-circle',
                    state: 'logout',
                    text: 'Logout'
                }
            ];
            expect($scope.userMenu).toEqual(menu);
        })
    })
    
    describe('guestCtrl', function() {
        
        it('should set guest menu', function() {
            var ctrl = $controller('guestCtrl', {$scope : $scope});
        
            var menu = [
                {
                    icon: 'fa fa-newspaper-o',
                    state: 'guest.news',
                    text: 'Last News'
                },

                {	
                    icon: 'fa fa-times-circle',
                    state: 'logout',
                    text: 'Login Page'
                }
            ];
            expect($scope.guestMenu).toEqual(menu);
        })
    })
    
    describe('newsCtrl', function() {
        var ctrl;
        
        beforeEach(function() {
            ctrl = $controller('newsCtrl', {$scope : $scope});
        })
        
        it('should clear searchFilter after clearInput is called', function() {
            
            $scope.searchFilter="something";
            $scope.clearInput();
            expect($scope.searchFilter).toEqual("");
        })
        
        it('should get data from server', function() {
            
            var art={news: [1,2,3,4]};
            var cat={categories: [5,6,7,8]};
            
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/news').respond(200, art);
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/categories/').respond(200, cat);    
            $httpBackend.flush();
            
            expect($scope.News).toEqual(art.news);
            expect($scope.Categories).toEqual(cat.categories);
        })
    })
    
    describe('topNewsCtrl', function() {
        var ctrl;
        
        beforeEach(function() {
            ctrl = $controller('topNewsCtrl', {$scope : $scope});
        })
        
        it('should clear searchFilter after clearInput is called', function() {
            
            $scope.searchFilter="something";
            $scope.clearInput();
            expect($scope.searchFilter).toEqual("");
        })
        
        it('should get data from server', function() {
            
            var art={news: [1,2,3,4]};
            var usr={user: {categories: [5,6,7,8]}};
            
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/news/top/').respond(200, art);
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(200, usr);          
            $httpBackend.flush();
            
            expect($scope.topNews).toEqual(art.news);
            expect($scope.topCategories).toEqual(usr.user.categories);
        })
    })
    
    describe('myNewsCtrl', function() {
        var ctrl;
        
        beforeEach(function() {
            ctrl = $controller('myNewsCtrl', {$scope : $scope});
        })
        
        it('should clear searchFilter after clearInput is called', function() {
            
            $scope.searchFilter="something";
            $scope.clearInput();
            expect($scope.searchFilter).toEqual("");
        })
        
        it('should get data from server', function() {
            
            var art={news: [1,2,3,4]};
            var usr={user: {categories: [5,6,7,8]}};
            
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/news/').respond(200, art);
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(200, usr);
            $httpBackend.flush();
            
            expect($scope.myNews).toEqual(art.news);
            expect($scope.myCategories).toEqual(usr.user.categories);
        })
    })
    
    describe('fullArticleCtrl', function() {
        var ctrl;
        var element;
        
        beforeEach(function() {
            $stateParams.articleId = 1;
            element = document.createElement('canvas');
            
            document.getElementById = jasmine.createSpy().and.callFake(function() {
                return element;
            });
            window.history.back = jasmine.createSpy();
            swal = jasmine.createSpy();
            console.log = jasmine.createSpy();
            
            ctrl=$controller('fullArticleCtrl', {$scope : $scope});
        })
        
        it('should get data from serwer', function() {
            var art={news: {id:1, up_votes:2, down_votes:3, voted:null}};
            
            $httpBackend.when('GET','http://news-world.iiar.pwr.edu.pl/api/v1/news/1').respond(200, art);
            $httpBackend.flush();
            
            expect($scope.article).toEqual(art.news);
            
            expect($scope.c).toEqual(element);
            expect($scope.ctx).toEqual($scope.c.getContext("2d"));
            expect($scope.l_length).toEqual(60);
            expect($scope.d_length).toEqual(90);
        })
        
        it('should call window.history.back, when backTo is called', function() {
            $scope.backTo();
            expect(window.history.back).toHaveBeenCalled();
        })
        
        it('like() function with voted=null', function() {
            var art={news: {id:1, up_votes:2, down_votes:3, voted:null}};
            
            $httpBackend.when('GET','http://news-world.iiar.pwr.edu.pl/api/v1/news/1').respond(200, art);
            $httpBackend.flush();
            $httpBackend.when('POST', 'http://news-world.iiar.pwr.edu.pl/api/v1/news/1/votes').respond(201);
            
            $scope.like();
            $httpBackend.flush();
            
            expect(swal).toHaveBeenCalledWith({ title: "You like this!", timer: 1500,type: "success", showConfirmButton: false});
            expect($scope.article.voted).toEqual(true);
        })
        
        it('like() function with voted=false', function() {
            var art={news: {id:1, up_votes:2, down_votes:3, voted:false}};
            
            $httpBackend.when('GET','http://news-world.iiar.pwr.edu.pl/api/v1/news/1').respond(200, art);
            $httpBackend.flush();
            $httpBackend.when('PUT', 'http://news-world.iiar.pwr.edu.pl/api/v1/news/1/votes').respond(200);
            
            $scope.like();
            $httpBackend.flush();
            
            expect(swal).toHaveBeenCalledWith({ title: "You like this!", timer: 1500,type: "success", showConfirmButton: false});
            expect($scope.article.voted).toEqual(true);
        })
        
        it('like() function with voted=true', function() {
            var art={news: {id:1, up_votes:2, down_votes:3, voted:true}};
            
            $httpBackend.when('GET','http://news-world.iiar.pwr.edu.pl/api/v1/news/1').respond(200, art);
            $httpBackend.flush();
            
            $scope.like();
            
            expect(console.log).toHaveBeenCalledWith("nie mozna glosowac 2 razy");
        })
        
        it('dislike() function with voted=null', function() {
            var art={news: {id:1, up_votes:2, down_votes:3, voted:null}};
            
            $httpBackend.when('GET','http://news-world.iiar.pwr.edu.pl/api/v1/news/1').respond(200, art);
            $httpBackend.flush();
            $httpBackend.when('POST', 'http://news-world.iiar.pwr.edu.pl/api/v1/news/1/votes').respond(201);
            
            $scope.dislike();
            $httpBackend.flush();
            
            expect(swal).toHaveBeenCalledWith({ title: "You dislike this!", timer: 1500,type: "error", showConfirmButton: false});
            expect($scope.article.voted).toEqual(false);
        })
        
        it('dislike() function with voted=true', function() {
            var art={news: {id:1, up_votes:2, down_votes:3, voted:true}};
            
            $httpBackend.when('GET','http://news-world.iiar.pwr.edu.pl/api/v1/news/1').respond(200, art);
            $httpBackend.flush();
            $httpBackend.when('PUT', 'http://news-world.iiar.pwr.edu.pl/api/v1/news/1/votes').respond(200);
            
            $scope.dislike();
            $httpBackend.flush();
            
            expect(swal).toHaveBeenCalledWith({ title: "You dislike this!", timer: 1500,type: "error", showConfirmButton: false});
            expect($scope.article.voted).toEqual(false);
        })
        
        it('dislike() function with voted=false', function() {
            var art={news: {id:1, up_votes:2, down_votes:3, voted:false}};
            
            $httpBackend.when('GET','http://news-world.iiar.pwr.edu.pl/api/v1/news/1').respond(200, art);
            $httpBackend.flush();
            
            $scope.dislike();
            
            expect(console.log).toHaveBeenCalledWith("nie mozna glosowac 2 razy");
        })
    })
    
    describe('adminManageArticlesCtrl', function() {
        var ctrl;
        
        beforeEach(function() {
            ctrl = $controller('adminManageArticlesCtrl', {$scope : $scope});
        })
        
        it('should clear searchFilter after clearInput is called', function() {
            
            $scope.searchFilter="something";
            $scope.clearInput();
            expect($scope.searchFilter).toEqual("");
        })
    
        it('should get data from server', function() {
            
            var art={news: [1,2,3,4]};
            var cat={categories: [5,6,7,8]};

            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/news/').respond(200, art);
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/categories/').respond(200, cat);
            $httpBackend.flush();
            
            expect($scope.ManageArticles).toEqual(art.news);
            expect($scope.Categories).toEqual(cat.categories);
        })
    })
    
    describe('adminManageUsersCtrl', function() {
        var ctrl;
        
        beforeEach(function() {
            ctrl = $controller('adminManageUsersCtrl', {$scope : $scope});
        })
        
        it('should clear searchFilter after clearInput is called', function() {
            
            $scope.searchFilter="something";
            $scope.clearInput();
            expect($scope.searchFilter).toEqual("");
        })
        
        it('should get data from server', function() {
            
            var usrs={users: [1,2,3,4]};
            var cat={categories: [5,6,7,8]};

            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/').respond(200, usrs);
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/categories/').respond(200, cat);
            
            $httpBackend.flush();
            
            expect($scope.ManageUsers).toEqual(usrs.users);
            expect($scope.Categories).toEqual(cat.categories);
        })
    })
    
    describe('setCategoryCtrl', function() {
        var ctrl;
        
        beforeEach(function() {
            swal = jasmine.createSpy();
            ctrl = $controller('setCategoryCtrl', {$scope : $scope});
        })
        
        it('should get data from serwer and set model', function() {
            
            var cat={categories: [5,6,7,8]};
            
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/categories/').respond(200, cat);
            $httpBackend.flush();
            
            expect($scope.Categories).toEqual(cat.categories);
            expect($scope.model).toEqual([false, false, false, false]);
        })
        
        it('sendCategory should show alert with success and redirect to /user/news if any of the categories were checked and http.put was successful', function() {
            
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/categories/').respond(200, {categories: [5,6,7,8]});
            $httpBackend.when('PUT', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(200);
            
            $scope.model=[false, true, false, false];
            $scope.Categories=[{id:1}, {id:2}, {id:3}, {id:4}];
            $scope.sendCategory($scope.model);
            
            $httpBackend.flush();
            
            expect(swal).toHaveBeenCalledWith("Well done", "You have set your preferences", "success");
            expect($location.path()).toEqual('/user/news');
        })
        
        it('sendCategory should show alert with error and redirect to /user/news if none of the categories were checked and http.put was successful', function() {
            
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/categories/').respond(200, {categories: [5,6,7,8]});
            $httpBackend.when('PUT', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(200);
            
            $scope.model=[false, false, false, false];
            $scope.Categories=[{id:1}, {id:2}, {id:3}, {id:4}];
            $scope.sendCategory($scope.model);
            
            $httpBackend.flush();
            
            expect(swal).toHaveBeenCalledWith("", "Please choose your category preferencess next time", "warning");
            expect($location.path()).toEqual('/user/news');
        })
    })
    
    describe('profileCtrl', function() {
        var ctrl;
        
        beforeEach(function() {
            swal = jasmine.createSpy();
            console.log = jasmine.createSpy();
            ctrl = $controller('profileCtrl', {$scope : $scope});
        })
        
        it('should get data from serwer, set session storage item and set model', function() {
            
            var usr={user: {email: 'userEmail'}};
            var cat={categories: [5,6,7,8]};
            
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/categories/').respond(200, cat);
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(200, usr);
            $httpBackend.flush();
            
            expect($scope.userData).toEqual(usr.user);
            expect($scope.Categories).toEqual(cat.categories);
            expect($scope.model).toEqual([false, false, false, false]);
            expect(sessionStorage.getItem('email')).toEqual('userEmail');
        })
        
        it('isChecked should return false if there are true elements in model', function() {
            $scope.model = [false, true, false, false];
            expect($scope.isChecked()).toEqual(false);
        })
        
        it('isChecked should return true if there are no true elements in model', function() {
            $scope.model = [false, false, false, false];
            expect($scope.isChecked()).toEqual(true);
        })
        
        it('editEmail should show alert with error if new email is the same as the old one', function() {
            
            sessionStorage.setItem('email', 'oldEmail');
            $scope.editEmail('oldEmail');
            expect(swal).toHaveBeenCalledWith("Error!", "You chose the same email!" , "error");
        })
        
        it('editEmail should show alert with success if new email is different and http.put was successful', function() {
            
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/categories/').respond(200, {categories: [5,6,7,8]});
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(200, {user: {email: 'userEmail'}});
            $httpBackend.when('PUT', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(200);
            
            sessionStorage.setItem('email', 'oldEmail');
            $scope.editEmail('newEmail');
            
            $httpBackend.flush();
            
            expect(swal).toHaveBeenCalledWith("Success!", "You successfully changed your email adress", "success");
        })
        
        it('editEmail should show alert with error if new email is different and http.put was not successful', function() {
            
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/categories/').respond(200, {categories: [5,6,7,8]});
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(200, {user: {email: 'userEmail'}});
            $httpBackend.when('PUT', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(400);
            
            sessionStorage.setItem('email', 'oldEmail');
            $scope.editEmail('newEmail');
            
            $httpBackend.flush();
            
            expect(swal).toHaveBeenCalledWith("Some Error!", "You need to fix your mistake", "error");
        })
        
        it('editPassword should show alert with error if new password is the same as the old one', function() {

            $scope.editPassword('currentPass', 'currentPass', 'currentPass');
            expect(swal).toHaveBeenCalledWith("Error!", "You chose the same password!" , "error");
        })
        
        it('editPassword should show alert with success if new password is different and http.post was successful', function() {
            
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/categories/').respond(200, {categories: [5,6,7,8]});
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(200, {user: {email: 'userEmail'}});
            $httpBackend.when('POST', 'http://news-world.iiar.pwr.wroc.pl/api/v1/users/me/password/').respond(201);
            
            $scope.editPassword('currentPass', 'newPass', 'newPass');
            
            $httpBackend.flush();
            
            expect(swal).toHaveBeenCalledWith("Success!", "You successfully changed your password", "success");
        })
        
        it('editPassword should show alert with error if new password is different and http.post was not successful', function() {
            
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/categories/').respond(200, {categories: [5,6,7,8]});
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(200, {user: {email: 'userEmail'}});
            $httpBackend.when('POST', 'http://news-world.iiar.pwr.wroc.pl/api/v1/users/me/password/').respond(400);
            
            $scope.editPassword('currentPass', 'newPass', 'newPass');
            
            $httpBackend.flush();
            
            expect(swal).toHaveBeenCalledWith("Some Error!", "Problem with communication to the server", "error");
        })
        
        it('editCategory should show alert with success if any of the categories were checked and http.put was successful', function() {
            
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/categories/').respond(200, {categories: [5,6,7,8]});
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(200, {user: {email: 'userEmail'}});
            $httpBackend.when('PUT', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(200);
            
            $scope.model=[false, true, false, false];
            $scope.Categories=[{id:1}, {id:2}, {id:3}, {id:4}];
            $scope.editCategory($scope.model);
            
            $httpBackend.flush();
            
            expect(swal).toHaveBeenCalledWith("", "Your category has been changed", "success");
        })
        
        it('editCategory should show log message if none of the categories were checked and http.put was successful', function() {
            
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/categories/').respond(200, {categories: [5,6,7,8]});
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(200, {user: {email: 'userEmail'}});
            $httpBackend.when('PUT', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(200);
            
            $scope.model=[false, false, false, false];
            $scope.Categories=[{id:1}, {id:2}, {id:3}, {id:4}];
            $scope.editCategory($scope.model);
            
            $httpBackend.flush();
            
            expect(console.log).toHaveBeenCalledWith('zero zmian kategorii');
        })
    })
    
    describe('registerCtrl', function() {      
        var ctrl;
        
        beforeEach(function() {
            ctrl = $controller('registerCtrl', {$scope : $scope});
        })
        
        
        it('should redirect to /login, if registration was successful', function() {

            p = $q.defer();
            p.resolve();
            $scope.signUp('login', 'email', 'password', 'password');
            $scope.$apply();
            expect($location.path()).toEqual('/login');
        })
        
        it('should redirect to /register, if registration is not successful', function() {

            p = $q.defer();
            p.reject();
            $scope.signUp('login', 'email', 'password', 'password');
            $scope.$apply();
            expect($location.path()).toEqual('/register');
        })
    })
    
    describe('logoutCtrl', function() {
        
        it('should redirect to /login', function() {
            var ctrl = $controller('logoutCtrl', {$scope : $scope});
            expect($location.path()).toEqual('/login');
        })
    })
    
    describe('loginCtrl', function() {
        var ctrl;
        
        beforeEach(function() {
            swal = jasmine.createSpy();
            ctrl=$controller('loginCtrl', {$scope : $scope});
        })
        
        it('should redirect to /admin/news, if users role is admin', function() {
            ValidationService.setUserData({role: 'admin'});
            p=$q.defer();
            p.resolve({response: 'response'});
            
            $scope.signIn('login', 'password');
            $scope.$apply();
            
            expect($location.path()).toEqual('/admin/news');
        })
        
        it('should redirect to /user/news, if users role is not admin and user has a category set', function() {
            ValidationService.setUserData({role: 'user', categories_set: [1,2,3]});
            p=$q.defer();
            p.resolve({response: 'response'});
            
            $scope.signIn('login', 'password');
            $scope.$apply();
            
            expect($location.path()).toEqual('/user/news');
        })
        
        it('should redirect to /setcategory, if users role is not admin and user does not have a category set', function() {
            ValidationService.setUserData({role: 'user'});
            p=$q.defer();
            p.resolve({response: 'response'});
            
            $scope.signIn('login', 'password');
            $scope.$apply();
            
            expect($location.path()).toEqual('/setcategory');
        })
        
        it('should show alert with error, if ValidationService.signIn was not successful', function() {
            ValidationService.setUserData({role: 'user'});
            p=$q.defer();
            p.reject({response: 'response'});
            
            $scope.signIn('login', 'password');
            $scope.$apply();
            
            expect(swal).toHaveBeenCalledWith("Some Error!", "You need fix your mistake", "error");
        })
    })
    
})