describe('service', function() {
    'use strict';
    
    var p, $httpBackend, ValidationService, httpRequestInterceptor, $httpProvider;
    
    beforeEach(function() {
        
        module('newsApp', function(_$httpProvider_) {
            $httpProvider = _$httpProvider_;
        });
        inject(function($q, _$httpBackend_, _ValidationService_, _httpRequestInterceptor_ /*_$httpProvider_*/) {
            p = $q.defer();
            $httpBackend = _$httpBackend_;
            ValidationService = _ValidationService_;
            httpRequestInterceptor = _httpRequestInterceptor_;
        })
    })
    
    describe('ValidationService', function() {
        
        it('signIn should return resolved promise, set user data and session storage item, if http.post was successful', function() {      
            var usr = {user: {session: {token: 'token'}, role: 'user'}};
            
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(200, usr);
            $httpBackend.when('POST', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/sign_in').respond(201, usr);
        
            p.resolve(usr);
            var r = ValidationService.signIn('login', 'password');
            $httpBackend.flush();
            
            expect(r).toEqual(p.promise);
            expect(ValidationService.userData).toEqual(usr.user);
            expect(sessionStorage.getItem('token')).toEqual(usr.user.session.token);
        })
        
        it('signIn should return rejected promise, if http.post was not successful', function() {
            var usr = {user: {session: {token: 'token'}, role: 'user'}};
            
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(200, usr);
            $httpBackend.when('POST', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/sign_in').respond(400, usr);
        
            p.reject(usr);
            var r = ValidationService.signIn('login', 'password');
            $httpBackend.flush();
            
            expect(r).toEqual(p.promise);
        })
        
        it('signUp should return resolved promise, if http.post was successful', function() {
            
            var usr = {user: {role: 'user'}};
            var res = {success: true}
            swal = jasmine.createSpy();
            
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(200, usr);
            $httpBackend.when('POST', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/sign_up').respond(201, res);
        
            p.resolve(res);
            var r = ValidationService.signUp('login', 'password');
            $httpBackend.flush();
            
            expect(swal).toHaveBeenCalledWith("Great Job!", "You created Your account", "success");
            expect(r).toEqual(p.promise);
        })
        
        it('signUp should return resolved promise, if http.post was successful', function() {
            
            var usr = {user: {role: 'user'}};
            var res = {success: false}
            swal=jasmine.createSpy();
            
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(200, usr);
            $httpBackend.when('POST', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/sign_up').respond(201, res);
        
            p.reject(res);
            var r = ValidationService.signUp('login', 'password');
            $httpBackend.flush();
            
            expect(swal).toHaveBeenCalledWith("Some Error!", "You need to fix your mistake", "error");
            expect(r).toEqual(p.promise);
        })
        
        it('signUp should return resolved promise, if http.post was successful', function() {
            
            var usr = {user: {role: 'user'}};
            var res = {success: true}
            
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(200, usr);
            $httpBackend.when('POST', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/sign_up').respond(400, res);
        
            p.reject(res);
            var r = ValidationService.signUp('login', 'password');
            $httpBackend.flush();
            
            expect(r).toEqual(p.promise);
        })
        
        it('isSignedIn should return resolved promise, if user has assigned role', function() {
            ValidationService.userData = {role: 'admin'};
            p.resolve();
            
            var r = ValidationService.isSignedIn();
            expect(r).toEqual(p.promise);
        })
        
        it('isSignedIn should return rejected promise if user does not have assigned role and there is no token i sessionStorage', function() {
            ValidationService.userData = {};
            sessionStorage.setItem('token', '');
            p.reject();
            
            var r = ValidationService.isSignedIn();
            expect(r).toEqual(p.promise);
        })
        
        it('isSignedIn should return resolved promise and set userData if there is token in sessionStorage', function() {
            ValidationService.userData = {};
            sessionStorage.setItem('token', 'token');
            var usr = {user: {role: 'user'}};
            
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(200, usr);
            
            p.resolve(usr.user);
            var r = ValidationService.isSignedIn();
            $httpBackend.flush();
            
            expect(r).toEqual(p.promise);
            expect(ValidationService.userData).toEqual(usr.user);
        })
        
        it('isSignedIn should return rejected promise if there is token in sessionStorage, but http.get was not successful', function() {
            ValidationService.userData = {};
            sessionStorage.setItem('token', 'token');
            var usr = {user: {role: 'user'}};
            
            $httpBackend.when('GET', 'http://news-world.iiar.pwr.edu.pl/api/v1/users/me/').respond(400, usr);
            
            p.reject(usr);
            var r = ValidationService.isSignedIn();
            $httpBackend.flush();
            
            expect(r).toEqual(p.promise);
        })
    })
    
    describe('InterceptorService', function() {
        
        it('httpProvider should have httpRequestInterceptor as an interceptor', function() {
            expect($httpProvider.interceptors).toContain('httpRequestInterceptor');
        })
        
        it('should not place a token in the http request headers if there is none in sessionStorage', function () {
            sessionStorage.setItem('token', '');
            var config = httpRequestInterceptor.request({headers: {} });
            expect(config.headers['Authorization']).toBe(undefined);
        });
        
        it('should place a token in the http request headers if there is one stored in sessionStorage', function() {
            sessionStorage.setItem('token', 'something');
            var config = httpRequestInterceptor.request({headers: {} });
            expect(config.headers['Authorization']).toEqual('Token something');
        })

    })
})